﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oef28._5Printer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PC printer = new PC();
        public MainWindow()
        {
            InitializeComponent();
            printer.AddPrinter(new Printer("Printer 1:"));
            printer.AddPrinter(new Printer("Printer 2:"));
            printer.AddPrinter(new Printer("Printer 3:"));
            printer.AddPrinter(new Printer("Printer 4:"));
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {

            if(!printer.Printers[0].Busy)
            {
                UpdatePrinters(imgPrinterOne, "Images/PrinterOpen.png", lblPrinterOne, 0,false);
            }
            else if (!printer.Printers[1].Busy)
            {
                UpdatePrinters(imgPrinterTwo, "Images/PrinterOpen.png", lblPrinterTwo, 1, false);
            }
            else if (!printer.Printers[2].Busy)
            {
                UpdatePrinters(imgPrinterDrie, "Images/PrinterOpen.png", lblPrinterDrie, 2, false);
            }
            else if (!printer.Printers[3].Busy)
            {
                UpdatePrinters(imgPrinterFour, "Images/PrinterOpen.png", lblPrinterFour,3, false);
            } else
            {
                MessageBox.Show("Alle printers zijn in gebruik");
            }
        }
        private void UpdatePrinters(Image img, string path, Label lbl, int index, bool reset)
        {
            img.Source = new BitmapImage(new Uri(path, UriKind.Relative));
            lbl.Content = printer.Printers[index].ToString();
            if (!reset)
            {
                printer.PrintAf();
            }
            else
            {
                printer.Printers[index].Reset();
            }
        }
        private void btnResetPrinter1_Click(object sender, RoutedEventArgs e)
        {
            if (printer.Printers[0].Busy)
            {
                UpdatePrinters(imgPrinterOne, "Images/PrinterClose.png", lblPrinterOne, 0, true);
            }  
        }

        private void btnResetPrinter2_Click(object sender, RoutedEventArgs e)
        {
            if (printer.Printers[1].Busy)
            {
                UpdatePrinters(imgPrinterTwo, "Images/PrinterClose.png", lblPrinterTwo, 1, true);
            }
        }

        private void btnResetPrinter3_Click(object sender, RoutedEventArgs e)
        {
            if (printer.Printers[2].Busy)
            {

                UpdatePrinters(imgPrinterDrie, "Images/PrinterClose.png", lblPrinterDrie, 2, true);
            }
        }
        private void btnResetPrinter4_Click(object sender, RoutedEventArgs e)
        {
            if (printer.Printers[3].Busy)
            {
                 UpdatePrinters(imgPrinterFour, "Images/PrinterClose.png", lblPrinterFour,3, true);
            }
        }
    }
    class Printer
    {
        private bool _busy;
        private string _naam;

        public Printer() { }
        public Printer(string naam)
        {
            _naam = naam;
        }
        public void Reset()
        {
            _busy = false;
        }
        public override string ToString() => !_busy == false ? $"{Naam} wachtende op een print opdracht" : $"{Naam} is bezig met een print opdracht";
        
        public bool Busy { get => _busy; set => _busy = value; }
        public string Naam { get => _naam; set => _naam = value; }
    }
    class PC : Printer
    {
        private List<Printer> _printers = new List<Printer>();

        public PC()
        {

        }
        public void AddPrinter(Printer p)
        {
            _printers.Add(p);
        }
        public bool PrintAf()
        {
            bool  b = false;
            for (int i = 0; i < Printers.Count; i++)
            {
                if (!Printers[i].Busy)
                {
                    Printers[i].Busy = true;
                    b = Printers[i].Busy;
                    break;
                }
            }
            return b;
        }
        public List<Printer> Printers { get => _printers; set => _printers = value; }
    }
}
